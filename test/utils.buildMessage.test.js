const assert = require('assert');
const builMessage = require('../utils/buildMessage');

describe.only('utils - builMessage', function () {
  describe('when receives na entity and action', function () {
    it('should return the respective message', function () {
      const result = builMessage('movie', 'create');
      const expect = 'movie created';
      assert.strictEqual(result, expect);
    });
  });

  describe('when receives an entity and an action and is a list', function () {
    it('should return the respective message with entity in plural', function () {
      const result = builMessage('movie', 'list');
      const expected = 'movies listed';
      assert.strictEqual(result, expected);
    });
  });
});
