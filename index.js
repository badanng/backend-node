const express = require('express');
const app = express();

const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');

const { config } = require('./config/index');
const moviesAPI = require('./routes/movies');
const notFoundHandler = require('./utils/middleware/notFoundHandler');
const {
  logErrors,
  wrapErrors,
  errorHandler
} = require('./utils/middleware/errorHandler');

//Cors
app.use(cors());

//Body Parser
app.use(bodyParser.json());

//Morgan Http Logger
app.use(morgan('tiny'));

//Routes API
moviesAPI(app);

//Catch 404
app.use(notFoundHandler);

//Errors Middlewares
app.use(logErrors);
app.use(wrapErrors);
app.use(errorHandler);

//Server
app.listen(config.port, () => {
  console.log(`Listening http://localhost:${config.port}`);
});